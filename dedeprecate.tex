\documentclass[article,a4paper,]{memoir}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{color}
\usepackage{ulem}
\usepackage[american]
           {babel}        % needed for iso dates
\usepackage[iso,american]
           {isodate}      % use iso format for dates
\usepackage{listings}
\lstset{language=C++}
\usepackage{url}
\usepackage{hyperref}
\hypersetup{
	colorlinks=true,
	urlcolor=blue,
	citecolor=black,
	linkcolor=black
}




% From the standard tex
%%--------------------------------------------------
%% Difference markups
\definecolor{addclr}{rgb}{0,.6,.6}
\definecolor{remclr}{rgb}{1,0,0}
\definecolor{noteclr}{rgb}{0,0,1}

\renewcommand{\added}[1]{\textcolor{addclr}{\uline{#1}}}
\newcommand{\removed}[1]{\textcolor{remclr}{\sout{#1}}}
\renewcommand{\changed}[2]{\removed{#1}\added{#2}}

\newcommand{\nbc}[1]{[#1]\ }
\newcommand{\addednb}[2]{\added{\nbc{#1}#2}}
\newcommand{\removednb}[2]{\removed{\nbc{#1}#2}}
\newcommand{\changednb}[3]{\removednb{#1}{#2}\added{#3}}
\newcommand{\remitem}[1]{\item\removed{#1}}

\newcommand{\ednote}[1]{\textcolor{noteclr}{[Editor's note: #1] }}
% \newcommand{\ednote}[1]{}

\linespread{1.25}
\setlrmarginsandblock{3.5cm}{2.5cm}{*}
\setulmarginsandblock{2.5cm}{*}{1}
\checkandfixthelayout 

\title{De-deprecating volatile compound operations}
\author{Paul M. Bendixen <paulbendixen@gmail.com>}
\date{2021-03-15}
\begin{document}
%\maketitle

\begin{tabular}{ll}
\textbf{Number:} & D2327R0 \\
\textbf{Title:} & \huge{\thetitle} \\
\textbf{Project:} & ISO JTC1/SC22/WG21: Programming Language C++ \\
\textbf{Audience:} & SG14, SG1, EWG, WG21 \\
\textbf{Date:} & \printdate{\thedate} \\
\textbf{Authors:} & \theauthor \\
\textbf{Contributors:} & Jens Maurer \\
			& Arthur O'Dwyer \\
			& Ben Saks \\
\textbf{Emails} & \href{mailto:paulbendixen@gmail.com}{paulbendixen@gmail.com} \\
\textbf{Reply to} & Paul M. Bendixen \\
\end{tabular}
\chapter*{Revision history}

\chapter{Introduction} % Simplify thoroughly
The C++ 20 standard deprecated many functionalities of the volatile keyword. This was due to P1152\cite{P1152R4}.
The reasoning is given in the R0 version of the paper\cite{P1152R0}.

The deprecation was not received too well in the embedded world as volatile is commonly used for communicating with 
peripheral devices in microcontrollers\cite{Wouter20}.

The purpose of this paper is to show what parts of the deprecation that are critical to the embedded domain,
and hopefully find a solution that satisfies the embedded community without sacrificing the work of P1152.

\chapter{Problem statement}
\section{Background}
One of the great advantages of C++ is its closeness to the machine on which it operates.
This enables C++ to be used in very constrained devices such as microcontrollers without any operating system.
These systems often operate by manipulating memory mapped registers often only touching a single bit.

While there are multiple ways to manipulate single bits in a memory location the most idiomatic way is something along 
the lines of the following:

\begin{lstlisting}
// In vendor supplied hardware abstraction layer
struct ADC {
	volatile uint8_t CTRL;
	volatile uint8_t VALUE;
	...
};
#define ADC_CTRL_ENABLE (1 << 3)


// in user code
ADC1->CTRL |= ADC_CTRL_ENABLE; // Start the ADC
...
ADC1->CTRL &= ~ADC_CTRL_ENABLE; // Stop the ADC
\end{lstlisting}

This is further amplified by the fact that vendors supply macros or inline functions for setting or clearing bits
utilizing this idiom, or may use it in provided code generators, as shown in the following example from a library
for the Energy Micro (now Silicon Labs) series of ARM microcontrollers:

\begin{lstlisting}
// Copyright 2012 Energy Micro EMLib from em_i2c.h
__STATIC_INLINE void I2C_IntDisable(I2C_TypeDef *i2c, uint32_t flags)
{
  i2c->IEN &= ~(flags);
}
\end{lstlisting}


It is clear from the previous example that this idiomatic usage of compound operations clashes with the deprecation of
compound operations on volatile. The argument against compound operations on volatile variables is that it leads the
programmer to believe that the operation itself becomes compounded and therefore atomic. While this is possible
(\url{https://godbolt.org/z/q5bn5K}) it depends on the platform and the compiler.

Now since there on some platforms can be a read, modify, write cycle it is possible that an interrupt would happen in
between the read and the write, however most embedded code that uses this idiom needs to take care of this by being
right by construction i.e. not bit-twiddeling variables that are used in the interrupt service routines (ISRs).
The problem being that there is currently \emph{no} way to do this in a guaranteed atomic manner (see also
\cite[section 3.4.1]{P2268}).

\section{Scope}
So what is the impact of deprecating compound operations on volatile?
A search of some of the more widely used embedded libraries show that while its use is not massive, it is in some way
in almost all hardware libraries for embedded systems available today.

The numbers below are done by using UNIX grep\footnote{The search was done using the command
\\\texttt{grep -re "[A-Z0\_9]\textbackslash{}+\textbackslash{}(\textbackslash{}[.*\textbackslash{}]\textbackslash{})*[ ]\textbackslash{}+[\&|]=" --include \textbackslash{}*.h .}}
to find usages of either \lstinline{|=} or \lstinline{&=} on variables with a code style usually associated with
volatile members  in the code bases of the respective libraries.
While this method isn't foolproof it does give an indicator of the problem.

\begin{table}[hb]
	\centering
\begin{tabular}{|l|r|}
	\hline
	Library & compound operations \\
	\hline
	\hline
Silabs Gecko SDK & 293 \\
AVR libc 2.0 & 205 \\
Raspberry pi Pico SDK & 13\\
\hline
\end{tabular}
\caption{Occurrences of compound operations on variables typically associated with volatile}
\end{table}

This illustrates the major point, not only is there a lot of code already written out there \emph{working as expected}
but the usage of this idiom also prevents adopters of C++ to use the C libraries provided with their toolchains.
% paragraph about switching to C++ by just changing compilers.

Furthermore, one of the most common ways to gradually change from C to C++ is by keeping the same code and compile it
with a C++ compiler. (See e.g. \cite[from 27:46]{embo}). This procedure involves modifying the code to be correct in 
C++ and would possibly involve moving compound volatile statements to non-compound statements. However the prevalence
of this idiom in C headers would prevent this low cost approach and would require a larger up front investment creating
new headers to replace the vendor supplied ones.

The idea that businesses will be willing to spend the effort to update existing codebases that become defunct because of
this changes seems unlikely, rather it seems likely that they will mandate using no newer versions.

\section{Didn't we just go over this?}
While the proposal to deprecate was heard in committee meetings, the main focus was on problems arising with 
multi-threaded code (SG1) and with EWG, neither of these groups can be expected to be familiar with the inner workings
on microcontrollers.

\chapter{Possible solutions}

\section{The simple}
The simplest possible change that could possibly work would be to remove the text added to paragraph [expr.ass] point 6
as this would allow compound statements on volatile variables.

\begin{quote}
The behavior of an expression of the form E1 op= E2 is equivalent to E1 = E1 op E2 except that E1 is evaluated only once.
\removed{Such expressions are deprecated if E1 has volatile-qualified type; see [depr.volatile.type].}
For += and -=, E1 shall either have arithmetic type or be a pointer to a possibly cv-qualified completely-defined object type.
In all other cases, E1 shall have arithmetic type.
\end{quote}

Since this brings in non-simple assignments to volatile-qualified operands, the previous paragraph should be modified:
\begin{quote}
	\changed{A simple}{An} assignment whose left operand is of a volatile-qualified type is deprecated ([depr.volatile.type]) unless the (possibly parenthesized) assignment is a discarded-value expression or an unevaluated operand.
\end{quote}

\section{The compromise}
As the compound operations are mainly used to flip bits, a compromise could be to only de-deprecate the use of binary
compound operations (\texttt{ |= \&= \^{}= } and possibly \texttt{ >>= <<=}) as these are the ones that are useful for this purpose. 

This would require new wording to be put in.

% Section on P2139
Built-in operators [over.built] point 25 would be affected in that only half the points would be affected.


\chapter{Impact}
The changes proposed by this paper would affect the current proposal P2139\cite{P2139}.
\chapter{Thanks}
Thanks to Jens Maurer for the suggestion on the compromise solution.

\bibliography{dedeprecate}
\bibliographystyle{apalike}
\end{document}

